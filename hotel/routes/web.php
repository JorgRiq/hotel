<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/booking', 'BookingController@index');

Route::get('/', function () {
    return view('home');
});

Route::get('/bookingDetails', 'BookingController@reservation');

Route::get('/admin', 'AdminController@index')->middleware('auth');

Route::get('/rooms', 'BookingController@rooms');
Route::get('/payment/{id}', 'BookingController@payment')->where('id', '[0-9]+');
Route::get('/admin/confirm/{id}', 'AdminController@confirm')->where('id', '[0-9]+')->middleware('auth');;
Route::get('/admin/delete/{id}', 'AdminController@delete')->where('id', '[0-9]+')->middleware('auth');;

Route::get('/endbooking', 'BookingController@end_booking');

Route::get('/confirmation/{id}', 'BookingController@confirm');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
