@extends('layouts.hotel')

@section('title', 'Reserva')

@section('content')
  <div class="ftco-section-reservation">
  <div class="container" style="text-align:center;">
  <div class="row ftco-animate">
  <div class="col-lg-4 col-md-3"></div>
  <div class="col-lg-4 col-md-6 reservation p-md-5">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
  <form method="get" class="d-block" action="{{action('BookingController@reservation')}}" >
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="fields d-block">
        <div>
          <label for="check-in">Full name:</label>
          <input type="text" name="name" id="name" class="form-control" placeholder="Name...">
        </div>
        <div>
          <label for="check-out">E-mail:</label>
          <input type="email" name="mail" id="mail" class="form-control" placeholder="something@domain.com">
        </div>
        <div>
          <label for="check-out">Phone number:</label>
          <input type="Phone" name="phone" id="phone" class="form-control" placeholder="XXX-XXX-XXX(Optional)">
        </div>
        <div>Date of birth:</label>
          <input type="date" name="dob" id="dob" class="form-control datepicker" placeholder="DD/MM/YYYY">
        </div>
      </div>
    <input type="submit" style="margin-top:20px" class="search-submit btn btn-primary" value="Continue">
  </form>
</div>
</div>
</div>
</div>
@endsection()
