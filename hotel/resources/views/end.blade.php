@extends('layouts.hotel')

@section('title', 'Reserva')

@section('componentcss')
  <link rel="stylesheet" href="{{ asset('css/payment.css') }}">
@endsection()

@section('content')
  <div class="ftco-section-reservation">
  <div class="container" style="text-align:center;">
  <div class="row ftco-animate">
  <div class="col-lg-2"></div>
  <div class="col-lg-8 col-md-6 p-md-5">
    <h1>Thanks for choosing us</h1>
    <h2>You'll receive soon an email for confirmation</h2>
  </div>
  </div>
  </div>
  </div>
@endsection()
