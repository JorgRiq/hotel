@extends('layouts.hotel')

@section('title', 'Reserva')

@section('content')
  <div class="ftco-section-reservation">
  <div class="container" style="text-align:center;">
  <div class="row ftco-animate">
  <div class="col-lg-4 col-md-3"></div>
  <div class="col-lg-4 col-md-6 reservation p-md-5">
  <form action="{{action('BookingController@rooms')}}" method="get" class="d-block">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="fields d-block">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div>
          <label for="check-in">Check in:</label>
          <input type="date" name="checkin_date" id="checkin_date" class="form-control datepicker" placeholder="DD/MM/YYYY">
        </div>
        <div>
          <label for="check-out">Check out:</label>
          <input type="date" name="checkout_date" id="checkout_date" class="form-control datepicker" placeholder="DD/MM/YYYY">
        </div>
        <div>
          <label for="Guest">Number of guests:</label>
          <div class="select-wrap">
            <div class="icon"><span class="ion-ios-arrow-down"></span></div>
            <select name="gente" id="gente" class="form-control">
              <option value=1>1</option>
              <option value=2>2</option>
              <option value=3>3</option>
              <option value=4>4+</option>
            </select>
          </div>
        </div>
      </div>
    <input style="margin-top:20px" type="submit" class="search-submit btn btn-primary" value="Check Availability">
  </form>
</div>
</div>
</div>
</div>
@endsection()
