@extends('layouts.hotel')

@section('title', 'Reserva')
@section('componentcss')
  <link rel="stylesheet" href="{{ asset('css/rooms.css') }}">
@endsection()

@section('content')
  <div class="ftco-section-reservation">
  <div class="container" style="text-align:center;">
  <div class="row ftco-animate">
  <div class="col-lg-2"></div>
  <div class="col-lg-8 col-md-6 p-md-5">
    <h1>Avaliable rooms:</h1>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="fields d-block">
        @foreach ($rooms as $room)
        <div class="item col-lg-3">
          <div class="room-wrap">
            <div class="text">
                <div class="d-flex mb-1">
                  <div class="col-lg-6">
                    <h4>Room</h4>
                    <h2>{{$room->id}}</h2>
                  </div>
                </div>
                <p class="features">
                  <span class="d-block mb-2"><i class="icon-check mr-2"></i>For {{$room->n_guests}} people</span>
                </p>
                <p><a href="/payment/{{$room->id}}" class="btn btn-primary center">Reserve for {{$room->r_price}}€</a></p>
            </div>
          </div>
        </div>
        @endforeach
      </div>
</div>
</div>
</div>
</div>
@endsection()
