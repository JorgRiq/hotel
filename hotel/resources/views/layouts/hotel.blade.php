<html>
  <head>
    <title>Hotel - @yield('title')</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/positions.css') }}" rel="stylesheet">
    <script href="{{ asset('js/effects.js') }}"></script>
    @yield('componentcss')
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="#">Hotel</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="/">Home </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/booking">Reservar</a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="img-beach">
      <span><h1>Bienvenido al Hotel</h1></span>
    </div>
    @yield('content')
</html>
