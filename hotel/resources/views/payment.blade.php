@extends('layouts.hotel')

@section('title', 'Reserva')

@section('componentcss')
  <link rel="stylesheet" href="{{ asset('css/payment.css') }}">
@endsection()

@section('content')
  <div class="ftco-section-reservation">
  <div class="container" style="text-align:center;">
  <div class="row ftco-animate">
  <div class="col-lg-2"></div>
  <div class="col-lg-8 col-md-6 p-md-5">
    <h1>Your total is: {{($price)}}</h1>
    <h2>Fill the following field to continue.</h2>
    <form method="get" class="d-block" action="{{action('BookingController@end_booking')}}" >
    <div class="fields d-block">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="f_item">
      <label for="pago">Metodo de pago:</label>
      <div class="select-wrap">
        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
        <select name="pago" id="pago" class="form-control">
          <option value="">Seleccione el metodo de pago</option>
          <option value="Credit card">Credit card</option>
          <option value="Transference">Transference</option>
          <option value="Payment at hotel">Payment at hotel</option>
        </select>
        </div>
      </div>
    </div>
      <div class="f_item">
        <label for="info">Informacion de pago:</label>
        <input name="info" class="form-control" id="info" type="text">
      </div>
      <div class="f_item">
        <h2>Comments:</h2>
        <div class="widget-area no-padding blank">
					<div >
							<textarea type="txt" class="comments" name="comment" placeholder="Any comments?" ></textarea>
					</div><!-- Status Upload  -->
			  </div>
      </div>
      <input style="margin-top:20px" type="submit" class="search-submit btn btn-primary" value="Reserve">
    </div>
    </form>
  </div>
  </div>
  </div>
  </div>
@endsection()
