@extends('layouts.hotel')
@section('title', 'Home')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card" style="margin-top: 25px;">
            <img class="card-img-top" src="{{asset('img/hotel.jpg')}}" alt="Hotel">
            <div class="card-body">
              Welcome to our wonderful hotel.<br>
              Please read our home page and make a booing when you feel ready.
            </div>
          </div>
          <div class="card" style="margin-top: 25px;">
            <img class="card-img-top" src="{{asset('img/buffet.jpg')}}" alt="Buffet">
            <div class="card-body">
              Check our buffet style restaurant.<br>
              We cook food from all cultures each day.<br>
              There's always something for everyone.
            </div>
          </div>
          <div class="card" style="margin-top: 25px;">
            <img class="card-img-top" src="{{asset('img/beach2.jpg')}}" alt="Beach">
            <div class="card-body">
              We have a wonderful beach within walking distance.<br>
              Bath play or just walk there any moment you desire.
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
