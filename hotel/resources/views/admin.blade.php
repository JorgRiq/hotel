@extends('layouts.hotel')

@section('title', 'Admin')

@section('content')
<div class="ftco-section-reservation">
<div class="container-fluid" style="text-align:center;">
<div class="row ftco-animate">
<div>
    <h1>Unconfirmed bookings:</h1>
    <table class="table table-striped table-responsive">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Room</th>
          <th scope="col">CheckIn</th>
          <th scope="col">CheckOut</th>
          <th scope="col">Guest</th>
          <th scope="col">Email</th>
          <th scope="col">Number</th>
          <th scope="col">DoB</th>
          <th scope="col">Payment method</th>
          <th scope="col">Payment info</th>
          <th scope="col">Comments</th>
          <th scope="col">Confirm</th>
          <th scope="col">Delete</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($bookingsuc as $booking)
        <tr>
          <th scope="row">{{$booking->id}}</th>
          <td>{{$booking->room}}</td>
          <td>{{date( "Y-m-d", strtotime($booking->datefrom))}}</td>
          <td>{{date( "Y-m-d", strtotime($booking->dateto))}}</td>
          <td>{{$booking->guest}}</td>
          <td>{{$booking->email}}</td>
          <td>{{$booking->pnumber}}</td>
          <td>{{date( "Y-m-d", strtotime($booking->dob))}}</td>
          <td>{{$booking->payment}}</td>
          <td>{{$booking->pinfo}}</td>
          <td>{{$booking->comments}}</td>
          <td><a href="/admin/confirm/{{$booking->id}}">Confirm</a></td>
          <td><a href="/admin/delete/{{$booking->id}}">Delete</a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <h1>Confirmed bookings:</h1>
    <table class="table table-striped table-responsive">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Room</th>
          <th scope="col">CheckIn</th>
          <th scope="col">CheckOut</th>
          <th scope="col">Guest</th>
          <th scope="col">Email</th>
          <th scope="col">Number</th>
          <th scope="col">DoB</th>
          <th scope="col">Payment method</th>
          <th scope="col">Payment info</th>
          <th scope="col">Comments</th>
          <th scope="col">Delete</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($bookingsc as $booking)
        <tr>
          <th scope="row">{{$booking->id}}</th>
          <td>{{$booking->room}}</td>
          <td>{{date( "Y-m-d", strtotime($booking->datefrom))}}</td>
          <td>{{date( "Y-m-d", strtotime($booking->dateto))}}</td>
          <td>{{$booking->guest}}</td>
          <td>{{$booking->email}}</td>
          <td>{{$booking->pnumber}}</td>
          <td>{{date( "Y-m-d", strtotime($booking->dob))}}</td>
          <td>{{$booking->payment}}</td>
          <td>{{$booking->pinfo}}</td>
          <td>{{$booking->comments}}</td>
          <td><a href="/admin/delete/{{$booking->id}}">Delete</a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
</div>
</div>
</div>
</div>
@endsection()
