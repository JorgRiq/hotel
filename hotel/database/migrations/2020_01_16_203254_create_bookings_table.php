<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room');
            $table->foreign('room')->references('id')->on('rooms')->onDelete('cascade');;
            $table->date('dateto');
            $table->date('datefrom');
            $table->string('guest');
            $table->string('email');
            $table->string('pnumber')->nullable();
            $table->date('dob');
            $table->string('payment');
            $table->string('pinfo')->nullable();
            $table->string('comments')->nullable();
            $table->boolean('confirmed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
