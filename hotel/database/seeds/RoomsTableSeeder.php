<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('rooms')->delete();

     \DB::table('rooms')->insert(array (
         0 =>
         array (
             'id' => 1,
             'n_guests' => 2,
             'r_price' => 30,
         ),
         1 =>
         array (
             'id' => 2,
             'n_guests' => 3,
             'r_price' => 35,
         ),
         2 =>
         array (
             'id' => 3,
             'n_guests' => 1,
             'r_price' => 20,
         ),
         3 =>
         array (
             'id' => 4,
             'n_guests' => 4,
             'r_price' => 40,
         ),
         4 =>
         array (
             'id' => 5,
             'n_guests' => 3,
             'r_price' => 35,
         ),
         5 =>
         array (
             'id' => 6,
             'n_guests' => 4,
             'r_price' => 40,
         ),
         6 =>
         array (
             'id' => 7,
             'n_guests' => 2,
             'r_price' => 30,
         ),
         7 =>
         array (
             'id' => 8,
             'n_guests' => 4,
             'r_price' => 40,
         ),
         8 =>
         array (
             'id' => 9,
             'n_guests' => 1,
             'r_price' => 20,
         ),
         9 =>
         array (
             'id' => 10,
             'n_guests' => 4,
             'r_price' => 40,
         ),
         10 =>
         array (
             'id' => 11,
             'n_guests' => 1,
             'r_price' => 20,
         ),
         11 =>
         array (
             'id' => 12,
             'n_guests' => 4,
             'r_price' => 40,
         ),
         12 =>
         array (
             'id' => 13,
             'n_guests' => 4,
             'r_price' => 40,
         ),
         13 =>
         array (
             'id' => 14,
             'n_guests' => 4,
             'r_price' => 40,
         ),
     ));
    }
}
