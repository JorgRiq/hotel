<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->delete();
      DB::table('users')->insert([
          'name' => 'John',
          'email' => 'user1@email.com',
          'password' => bcrypt('password'),
      ]);
      DB::table('users')->insert([
          'name' => 'James',
          'email' => 'user2@email.com',
          'password' => bcrypt('password2'),
      ]);
    }
}
