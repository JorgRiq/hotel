<?php
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BookingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('bookings')->delete();

     \DB::table('bookings')->insert(array (
         0 =>
         array (
             'id' => 1,
             'room'=>2,
             'datefrom'=>Carbon::parse('2020-01-20'),
             'dateto'=>Carbon::parse('2020-01-30'),
             'guest'=>'John Smith',
             'email'=>'something@something.com',
             'pnumber'=>'900900900',
             'dob' =>Carbon::parse('2000-01-01'),
             'payment'=>'Credit card',
             'pinfo'=>'2000123456781234',
             'comments'=> 'asdf',
             'confirmed'=>false,
         ),
         1 =>
         array (
             'id' => 2,
             'room'=>4,
             'datefrom'=>Carbon::parse('2020-01-25'),
             'dateto'=>Carbon::parse('2020-01-30'),
             'guest'=>'John Black',
             'email'=>'something_else@something.com',
             'pnumber'=>'900900906',
             'dob' =>Carbon::parse('1997-01-01'),
             'payment'=>'Transference',
             'pinfo'=>'ES242000123456781234',
             'comments'=> 'More candies',
             'confirmed'=>false,
         ),
     ));
    }
}
