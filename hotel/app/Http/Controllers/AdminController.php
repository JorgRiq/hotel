<?php

namespace App\Http\Controllers;
use App\Booking;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookingsuc=Booking::where('confirmed',0)->get();
        $bookingsc=Booking::where('confirmed',1)->get();
        return view('admin')->with('bookingsuc',$bookingsuc)->with('bookingsc',$bookingsc);
    }

    public function delete($id)
    {
        $booking=Booking::find($id);
        $booking->delete();
        return redirect('/admin');
    }

    public function confirm($id)
    {
        $booking=Booking::find($id);
        $booking->confirmed=true;
        $booking->save();
        return redirect('/admin');
    }
}
