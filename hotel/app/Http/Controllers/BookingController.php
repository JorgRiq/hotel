<?php

namespace App\Http\Controllers;
use App\Room;
use App\User;
use App\Booking;
use App\Confirmation;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    public function index()
    {
        return view('profile');
    }

    public function reservation(Request $request)
    {
        $request->validate([
          'name' => 'required',
          'mail' => 'required|email',
          'phone' => 'nullable|numeric',
          'dob' => 'required'
        ]);
        $request->session()->put('name', $request->name);
        $request->session()->put('mail', $request->mail);
        $request->session()->put('phone', $request->phone);
        $request->session()->put('dob', $request->dob);
        return view('reserva');
    }

    public function rooms(Request $request)
    {
      if($request->session()->has('name'))
      {
        $request->validate([
          'checkin_date' => 'required|after:today',
          'checkout_date' => 'required|after:checkin_date',
          'gente' => 'required|numeric'
        ]);
        $request->session()->put('checkin', $request->checkin_date);
        $request->session()->put('checkout', $request->checkout_date);
        $request->session()->put('gente', $request->gente);
        $bookings=Booking::where('datefrom','<', $request->checkout_date)->orWhere('dateto','>', $request->checkin_date)->select('room')->get()->toArray();
        $rooms=Room::where('n_guests',$request->session()->get('gente'))->whereNotIn('id',$bookings)->get();
        return view('room')->with('rooms',$rooms);
      }
      return view('profile');

    }

    public function payment(Request $request,$id)
    {
      if($request->session()->has('name') and $request->session()->has('checkin'))
      {
        $request->session()->put('id_room', $id);
        $checkout=date_create_from_format('Y-m-d', session()->get('checkout'));
        $checkin=date_create_from_format('Y-m-d', session()->get('checkin'));
        $room=Room::find($id);
        $diff=$room->r_price*$checkout->diff($checkin)->format("%d");
        return view('payment')->with('price',$diff);
      }
      return view('profile');
    }

    public function end_booking(Request $request)
    {
      if($request->session()->has('id_room'))
      {
        $request->session()->put('pago', $request->pago);
        $request->session()->put('info', $request->info);
        $request->session()->put('comment', $request->comment);
        $booking=new Booking;
        $booking->room=$request->session()->get('id_room');
        $booking->datefrom=$request->session()->get('checkout');
        $booking->dateto=$request->session()->get('checkin');
        $booking->guest=$request->session()->get('name');
        $booking->pnumber=$request->session()->get('phone');
        $booking->email=$request->session()->get('mail');
        $booking->dob=$request->session()->get('dob');
        $booking->payment=$request->session()->get('pago');
        $booking->pinfo=$request->session()->get('info');
        $booking->comments=$request->session()->get('comment');
        $booking->confirmed=false;
        $booking->save();

        $random_hash = md5(uniqid(rand(), true));
        $confirm=new Confirmation;
        $confirm->hash=$random_hash;
        $confirm->booking=$booking->id;
        $confirm->save();

        $to_name = $request->session()->get('name');
        $to_email = $request->session()->get('mail');
        $data = array('name'=>$to_name, "body" => "You have reserved a room at our hotel. Go to hostname:port/confirmation/".$random_hash." to confirm");
        Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('Hotel');
            $message->from('jorgriq97@gmail.com','Hotel Web');
        });
        $request->session()->flush();
        return view('end');
      }
      return view('profile');
    }

    public function confirm($id){
      $conf=Confirmation::where('hash',$id)->first();
      if($conf){
        $book=Booking::find($conf->booking)->first();
        $book->confirmed=true;
        $book->save();
        $conf->delete();
      }
      return view('thanks');
    }
}
