<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
  public $primaryKey = "id";
  protected $table = "rooms";
  protected $fillable = ['id','n_guests', 'r_price'];

  public function comments()
    {
        return $this->hasMany('App\Booking');
    }
}
