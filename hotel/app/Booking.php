<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
  public $primaryKey = "id";
  protected $table = "bookings";
  protected $fillable = ['id','room', 'datefrom','dateto','guest','email','pnumber','dob','payment','pinfo','comments','confirmed'];
  public function room()
    {
        return $this->hasOne('App\Room');
    }
}
