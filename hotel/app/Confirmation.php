<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Confirmation extends Model
{
  public $primaryKey = "id";
  protected $table = "confirmation";
  protected $fillable = ['id','booking', 'hash'];
}
