# Hotel

Hotel challenge 

Steps:
1-Prepare the enviroment to suppor laravel with a version of php of at least 7.2 https://laravel.com/docs/5.8/installation

2-Prepare the enviroment to support npm and node https://blog.risingstack.com/node-js-windows-10-tutorial/

3-Run npm install to get all the js packages used in the project

4-Prepare php artian migrate:refresh and php artisna db:seed to get some initial data

5-Configure email as desired to use a different one if needed.

6-Run php artisan serve to run the webpage in a local server.